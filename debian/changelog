golang-github-hashicorp-go-uuid (1.0.3-1+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 19:16:03 +0000

golang-github-hashicorp-go-uuid (1.0.3-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * d/control:
    - Update Section to golang
    - Update debhelper-compat to v13
    - Remove ${shlibs:Depends} from Depends
    - Bump Standards-Version to 4.6.2 (no changes needed)
  * Update years in d/copyright

 -- Mathias Gibbens <gibmat@debian.org>  Wed, 18 Oct 2023 23:11:47 +0000

golang-github-hashicorp-go-uuid (1.0.2-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Sat, 01 Apr 2023 01:23:01 +0000

golang-github-hashicorp-go-uuid (1.0.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Apply multi-arch hints. + golang-github-hashicorp-go-uuid-dev: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 29 Nov 2022 00:24:38 +0000

golang-github-hashicorp-go-uuid (1.0.2-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 12 Mar 2021 01:38:56 +0000

golang-github-hashicorp-go-uuid (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Rules-Requires-Root: no.
  * Standards-Version: 4.5.0.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sat, 29 Feb 2020 01:11:38 +1100

golang-github-hashicorp-go-uuid (1.0.1-2) unstable; urgency=medium

  * Standards-Version: 4.4.1
  * DH to version 12

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 29 Dec 2019 15:18:59 +1100

golang-github-hashicorp-go-uuid (1.0.1-1) unstable; urgency=medium

  * New upstream version 1.0.1
  * Bump Standards-Version to 4.4.0 (no change)

 -- Anthony Fok <foka@debian.org>  Sat, 27 Jul 2019 09:18:23 -0600

golang-github-hashicorp-go-uuid (1.0.0-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:58:17 +0000

golang-github-hashicorp-go-uuid (1.0.0-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Anthony Fok ]
  * New upstream version 1.0.0
  * Add debian/watch
  * Apply "cme fix dpkg" fixes to debian/control and debian/copyright,
    Adding "Testsuite: autopkgtest-pkg-go" field, setting Priority to optional,
    bumping Standards-Version to 4.2.1, bumping debhelper version to (>= 11~),
    and using secure https URL in debian/copyright, etc.
  * Update Maintainer address in debian/control
  * Add myself to the list of Uploaders
  * Update debian/gbp.conf to do without a build-area directory

 -- Anthony Fok <foka@debian.org>  Wed, 19 Dec 2018 23:05:10 -0700

golang-github-hashicorp-go-uuid (0.0~git20160311.0.d610f28-2) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Team upload.
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Konstantinos Margaritis ]
  * Replace golang-go with golang-any in Build-Depends, remove golang-go from
    Depends

 -- Konstantinos Margaritis <markos@debian.org>  Tue, 08 Aug 2017 16:07:56 +0300

golang-github-hashicorp-go-uuid (0.0~git20160311.0.d610f28-1) unstable; urgency=medium

  * Initial release (Closes: #819035).

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 23 Mar 2016 17:16:57 +1100
